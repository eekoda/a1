public class Sheep {

   enum Animal {sheep, goat};

   public static void main (String[] param) {
      System.out.println("fds");
   }

   /**
    *  Sorteerib array of Animal[] animals.
    * @autor Eerik Kodasma
    * @param animals
    */
   public static void reorder (Animal[] animals) {
      int amountOfAnimals = animals.length;

      // Counting goats.
      int howManyGoat = countGoats(animals);

      // Adding goats then sheeps.
      for (int i = 0; i < animals.length; i++) {
         if (howManyGoat > 0) {
            animals[i] = Animal.goat;
            howManyGoat--;
         } else {
            animals[i] = Animal.sheep;
         }
      }
   }

   /**
    * Counting goats.
    * @autor Eerik Kodasma
    * @param animals
    * @return How many goats are in array.
    */
   private static int countGoats(Animal[] animals) {
      int howManyGoat = 0;
      for (Animal animal: animals) {
         if (animal == Animal.goat) {
            howManyGoat++;
         }
      }
      return howManyGoat;
   }
}
